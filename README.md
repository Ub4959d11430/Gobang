# Gobang

A simple Gobang game written in Python.

## Requirements

- Python 3.x
- Pygame

## Usage

```
python main.py
```